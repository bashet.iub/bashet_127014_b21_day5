<?php
	 $a = 22;
	 $b = 12;
	 
	 $c = $a + $b;   /* Assignment operator */
	 echo $c;//34
	 echo "<br/>";
	 
	 $c += $a;  /* c value was 42 + 20 = 62 */
	 echo $c;//56
	 echo "<br/>";
	 
	 $c -= $a; /* c value was 42 + 20 + 42 = 104 */
	 echo $c;//34
	 echo "<br/>";
	 
?>